import java.util.Arrays;
import java.util.Scanner;
public class Assignment2 					//Lou Cuenca CSC130
{	public static void main(String[] args) 
	{
	System.out.println("Enter 5 integers pressing ENTER in between each one.");
	Scanner keyboard = new Scanner(System.in);
	int num1 = keyboard.nextInt();  			//The user will enter 5 numbers and this will assign them to a variable.
	int num2 = keyboard.nextInt();
	int num3 = keyboard.nextInt();
	int num4 = keyboard.nextInt();
	int num5 = keyboard.nextInt();
	
	int sum = num1+num2+num3+num4+num5;			//sum
	int ave = sum /5;					//average
	int min1 = (Math.min(num1, num2));			/*this is to find the minimum, not the best method but I made it work*/
	int min2 = (Math.min(num3, num4));	
	int min3 = (Math.min(num5, min1));
	int min4 = (Math.min(min3, min2));
	int min5 = (Math.min(min4, min3));
	
	int max1 = (Math.max(num1, num2));			/*this is to find the maximum, not the best method but I made it work*/
	int max2 = (Math.max(num3, num4));		
	int max3 = (Math.max(num5, max1));
	int max4 = (Math.max(max3, max2));
	int max5 = (Math.max(max4, max3));
	
	int[] allnumbers = {num1, num2, num3, num4, num5}; 	//using an array to locate the median
	Arrays.sort(allnumbers);
	int middle = allnumbers.length/2;
	int medianValue = 0; 
	if (allnumbers.length%2 == 1) 
	    medianValue = allnumbers[middle];
	else
	   medianValue = (allnumbers[middle-1] + allnumbers[middle]) / 2;
	
	int maxValue=0, maxCount = 0;				//to calculate the mode
	for (int i = 0; i < allnumbers.length; ++i) 
	{
        int count = 0;
        
        for (int j = 0; j < allnumbers.length; ++j) 
        {
			if (allnumbers[j] == allnumbers[i]) ++count;
        }
        if (count > maxCount)  
        {
            maxCount = count;
            maxValue = allnumbers[i];
        }}
           
	System.out.println("The sum is " +sum);			//All of the output
	System.out.println("The Average is " + ave);
	System.out.println("The smallest number is " + min5 );
	System.out.println("The largest number is " + max5);
	System.out.println("The median is " + medianValue);
	System.out.println("The mode is "+ maxValue);
		}

}